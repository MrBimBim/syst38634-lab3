import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidoterTest {

	@Test
	public void testCheckPasswordLengthRegular() {
		boolean check = PasswordValidoter.CheckPasswordLength("peothgjtidus");
		assertTrue("There are 8 or more characters", check == true);
	}
	@Test
	public void testCheckPasswordLengthException() {
		boolean check = PasswordValidoter.CheckPasswordLength("");
		assertTrue("There are less than 8 characters", check == false);
	}
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		boolean check = PasswordValidoter.CheckPasswordLength("12345678");
		assertTrue("There are 8 or more characters", check == true);
	}
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		boolean check = PasswordValidoter.CheckPasswordLength("1234567");
		assertTrue("There are less than 8 characters", check == false);
	}

	@Test
	public void testCheckNumberOfDigitsRegular() {
		boolean check = PasswordValidoter.CheckNumberOfDigits("thecat1234");
		assertTrue("There are more 2 or more digits", check == true);
	}
	@Test
	public void testCheckNumberOfDigitsException() {
		boolean check = PasswordValidoter.CheckNumberOfDigits("thecat");
		assertTrue("There are less than 2 digits", check == false);
	}
	@Test
	public void testCheckNumberOfDigitsBoundaryIn() {
		boolean check = PasswordValidoter.CheckNumberOfDigits("thecat12");
		assertTrue("There are 2 or more digits", check == true);
	}
	@Test
	public void testCheckNumberOfDigitsBoundaryOut() {
		boolean check = PasswordValidoter.CheckNumberOfDigits("thecat1");
		assertTrue("There are less than 2 digits", check == false);
	}

}
